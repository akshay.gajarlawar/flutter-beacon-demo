/*
* Android *
For Android app, user needs to turn on Bluetooth on the device first.
Android beacon will advertise as the AltBeacon manufactured by RadiusNetwork.
You can change it with setLayout() and setManufacturerId() methods.
*/

/*
* iOS *
Beacon will advertise as an iBeacon, it can't be changed.
It's worth to mention that application needs to work in foreground.
According to the CoreLocation documentation:
After advertising your app as a beacon, your app must continue running in the foreground to broadcast the needed Bluetooth signals.
If the user quits the app, the system stops advertising the device as a peripheral over Bluetooth.
*/


import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:beacon_broadcast/beacon_broadcast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:foreground_service/foreground_service.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() {
  runApp(MyApp());
  initializeNotification();
}

void initializeNotification() async {
  try {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: didSelectNotification);
  } catch(e) {
    print(e.toString());
  }
}

Future<void> didSelectNotification(String payload) async {
  if (payload != null) {
    debugPrint('notification payload: ' + payload);
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Beacon Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final beaconBroadcast = BeaconBroadcast();

  final uuid = 'E1621B58-00BB-AF0A-BD10-C15C1F05CF99';
  final identifier = 'com.beacon.demo';
  int major;
  int minor;

  var broadcastMsg = '';
  StreamSubscription<RangingResult> _streamRanging;
  StreamSubscription<MonitoringResult> _streamMonitoring;

  Map<String, double> beacons = {};
  Map<String, List<double>> readings = {};
  int readingCount = 5;
  double maxDistance = 3.0;

  bool locked = false;

  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'A1B2C3', 'Beacon', 'Device Detection',
      importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();

  @override
  void initState() {
    super.initState();
    initialSetup();
    listeningState();
    initializeBroadcast();
    initializeScan();
    if(Platform.isAndroid) {
      setForegroundService();
    }
  }

  void initialSetup() {
    major = getRandomNumber();
    minor = getRandomNumber();

    beaconBroadcast.setUUID(uuid)
        .setMajorId(major)
        .setMinorId(minor);

    if(Platform.isIOS) {
      beaconBroadcast.setIdentifier(identifier);
    }
    else {
      //Let this be iBeacon with Apple manufacture id
      //This is imp in order to let other device detect this device
      beaconBroadcast.setLayout('m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24')
          .setManufacturerId(0x004C);
    }
  }


  int getRandomNumber() {
    int min = 1000;
    int max = 9999;
    final rand = new Random();
    return min + rand.nextInt(max - min);
  }

  listeningState() async {
    print('Listening to bluetooth state');
    flutterBeacon
        .bluetoothStateChanged()
        .listen((BluetoothState state) async {
      switch (state) {
        case BluetoothState.stateOn:
          initializeBroadcast();
          initializeScan();
          break;
        case BluetoothState.stateOff:
          setState(() {
            broadcastMsg = 'Please turn on the bluetooth';
          });
          break;
      }
    });
  }

  void initializeBroadcast() async {

    broadcastMsg = 'Checking Status ...';

    var msg = '';

    var transmissionSupportStatus = await beaconBroadcast.checkTransmissionSupported();
    switch (transmissionSupportStatus) {
      case BeaconStatus.SUPPORTED:
        startBroadcast();
        msg = 'Started Broadcasting!';
        break;

      case BeaconStatus.NOT_SUPPORTED_MIN_SDK:
        msg = "Your Android system version is too low.";
        break;

      case BeaconStatus.NOT_SUPPORTED_BLE:
        msg = "Your device doesn't support BLE.";
        break;

      case BeaconStatus.NOT_SUPPORTED_CANNOT_GET_ADVERTISER:
        msg = "Please check your bluetooth is turned on and try again.";
        break;
    }

    setState(() {
      broadcastMsg = msg;
    });
  }

  void startBroadcast() {
    beaconBroadcast.start();
    beaconBroadcast.getAdvertisingStateChange().listen((isAdvertising) {
      setState(() {
        broadcastMsg = isAdvertising ? 'Broadcasting' : 'Stopped';
      });
    });
  }

  void initializeScan() async {
    try {
      await flutterBeacon.initializeAndCheckScanning;
      this.startRanging();
    } catch(e) {
      print(e);
    }
  }

  void startRanging()
  {
    if (_streamRanging != null) {
      if (_streamRanging.isPaused) {
        _streamRanging.resume();
      }
      return;
    }

    this.readings = {};
    this.beacons = {};

    final regions = <Region>[
      Region(identifier: Platform.isIOS ? identifier : null, proximityUUID: uuid)
    ];


    _streamRanging = flutterBeacon.ranging(regions).listen((RangingResult result) {
      if(locked) {
        return;
      }
      //TODO: Handling for background
      //Problem: result.beacons.length is 0 after few minutes app sent to background
      //Foreground Service: I've implemented a service, see foregroundServiceFunction() below, but its not working
      setReadings(result.beacons ?? []);
    });
  }


  void setReadings(List<Beacon> resultBeacons)
  {
    locked = true;
    var processReads = false;

    if(resultBeacons.length > 0)
    {
      resultBeacons.forEach((beacon) {
        final str = getMajorMinorAsString(beacon);
        if(readings.containsKey(str)) {
          final list = readings[str];
          list.add(beacon.accuracy);
          if(list.length == readingCount) {
            processReads = true;
            processReadings(list, str);
            readings.remove(str);
          }
        } else {
          readings[str] = [beacon.accuracy];
        }
      });
    }
    else if (readings.length > 0) {

      List<String> keysToRemove = [];

      readings.forEach((key, value) {
        var list = value;
        list.add(0);
        if(list.length == readingCount) {
          processReads = true;
          processReadings(list, key);
          keysToRemove.add(key);
        } else {
          readings[key] = list;
        }
      });

      if(keysToRemove.length > 0) {
        keysToRemove.forEach((element) {
          readings.remove(element);
        });
      }
    }

    if(!processReads) {
      locked = false;
    }
  }

  String getMajorMinorAsString(Beacon beacon) {
    return beacon.major.toString() + '-' + beacon.minor.toString();
  }

  void processReadings(List<double> reads, String mmString) {

    double averageDistance = 0;

    reads.forEach((dist) {
      averageDistance += dist;
    });

    averageDistance = averageDistance/readingCount;

    if(averageDistance > 0 && averageDistance <= maxDistance) {
      if(!this.beacons.containsKey(mmString)) {
        setState(() {
          this.beacons[mmString] = averageDistance;
        });
        showLocalNotification(mmString, averageDistance);
      }
    }
    else if(this.beacons.containsKey(mmString)) {
      setState(() {
        this.beacons.remove(mmString);
      });
    }

    ForegroundService.notification.setText("Nearby: ${this.beacons.length} Devices");

    locked = false;
  }

  void showLocalNotification(String mmString, double distance) async {
    try {
      var platformChannelSpecifics = NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          0, 'Someone is close to you', 'at approx. ${distance}m', platformChannelSpecifics,
          payload: 'item x');
    }
    catch(e) {
      print(e.toString());
    }
  }

//use an async method so we can await
  void setForegroundService() async {
    try {
      if (!(await ForegroundService.foregroundServiceIsStarted())) {
        await ForegroundService.setServiceIntervalSeconds(1);

        //necessity of editMode is dubious (see function comments)
        await ForegroundService.notification.startEditMode();
        await ForegroundService.notification
            .setTitle("Beacon Demo");
        await ForegroundService.notification
            .setText("Scanning");
        await ForegroundService.notification.finishEditMode();

        await ForegroundService.startForegroundService(foregroundServiceFunction);
        await ForegroundService.getWakeLock();
      }

      ///this exists solely in the main app/isolate,
      ///so needs to be redone after every app kill+relaunch
      await ForegroundService.setupIsolateCommunication((data) {
        debugPrint("main received: $data");
      });
    }
    catch (e) {
      startRanging();
      print(e.toString());
    }
  }

  void foregroundServiceFunction() {
    print('***** STARTING FOREGROUND SERVICE *****');
    this.startRanging();
    if (!ForegroundService.isIsolateCommunicationSetup) {
      ForegroundService.setupIsolateCommunication((data) {
        debugPrint("bg isolate received: $data");
      });
    }
    ForegroundService.sendToPort("message from bg isolate");
  }

  @override
  void setState(fn) {
    if(this.mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: 16.0),
          Text('$major-$minor', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
          SizedBox(height: 4.0),
          Text(broadcastMsg ?? '', textAlign: TextAlign.center,),
          SizedBox(height: 16.0),
          Container(height: 1.0, color: Colors.grey),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Nearby Devices:', style: TextStyle(fontWeight: FontWeight.w500),),
          ),
          Expanded(child: this.beacons.length > 0 ? listView() : Center(child: Text('No Device Found!')))
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget listView() {

    List<String> allKeys = this.beacons.keys.toList();

    return ListView.builder(
        itemCount: allKeys.length,
        itemBuilder: (BuildContext context, int index) {
          final key = allKeys[index];
          final value = this.beacons[key];
          return Card(
            child: ListTile(
              title: Text(key),
              subtitle: Text('Distance: ${value.toString()} m'),
            ),
          );
        });
  }

  /*
  void _toggleForegroundServiceOnOff() async {
    final fgsIsRunning = await ForegroundService.foregroundServiceIsStarted();
    String appMessage;

    if (fgsIsRunning) {
      await ForegroundService.stopForegroundService();
      appMessage = "Stopped foreground service.";
    } else {
      maybeStartFGS();
      appMessage = "Started foreground service.";
    }

    setState(() {
      _appMessage = appMessage;
    });
  }
   */
}
